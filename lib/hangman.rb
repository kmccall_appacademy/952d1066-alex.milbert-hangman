class Hangman #holds your game n
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
  end

  def play
    setup
    take_turn until over?
    conclude
  end

  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)  #STUDY
    @board = [nil] * secret_length
  end

  def take_turn
    guess = @guesser.guess(board)
    indices = @referee.check_guess(guess)
    update_board(guess, indices)
    @guesser.handle_response(guess, indices)
  end

  private

  def update_board(indices, letter)
    indices.each {|idx| @board[idx] = letter}
  end

  def over?
    @board.count(nil) == 0
  end

  def conclude
    puts "Congrats!! on #{@board.join('')}"
  end


end

class HumanPlayer #q's and a's  edge cases for human error

  def initialize
    @guessed_letters = []
  end

  def register_secret_length(length)
    puts "Secret word is #{length} long"
  end

  def guess(board)
    print_board
    print "> "
    gets.chomp
    process_guess
  end

  def pick_secret_word
    print "Enter seceret work length:"
    gets.chomp.to_i

  end

  def check_guess(letter)

    puts "comp guess #{letter}"
    print "Positions(0 2 4)"
    gets.chomp.split.map(&:to_i)
  end



  private

  def process_guess(guess)
    unless @guessed_letters.include?(guess)
      @guessed_letters << guess
      return guess
    else
      puts "You already guess #{guess}"
      puts "You have already guessed:"
      p @guessed_letters
      puts "Please guess again"
      print "> "
      guess = gets.chomp.downcase
      process_guess(guess)
    end

  end

  def print_board(board)
    board_string = board.map do |el|
      el.nil? ? "_" : el
    end.join("")
    puts "Secret word: #{board_string}"
  end

end

class ComputerPlayer

  attr_reader :candidate_words

  def self.read_dictionary
    File.readlines('dictionary.txt').map(&:chomp)
  end

  def initialize(dictionary)
    @dictionary = dictionary
    @alphabet = ("a".."z").to_a
  end

  def candidate_words
    @dictionary
  end

  def pick_secret_word
    p @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    indices = []
    @secret_word.each_char.with_index do |char, idx|
      indices << idx if char == letter
    end
    indices
  end

  def register_secret_length(length)
    @dictionary.select! {|word| word.length == length}
  end

  def handle_response(letter, indices)

    @dictionary.select! do |word|
      word_indices = []
      word.chars.each_with_index do |char, idx|
        word_indices << idx if char == letter
      end
      indices == word_indices
    end
  end

  def guess(board)
    best = @alphabet.first
    best_count = 0
    @alphabet.each do |letter|
      count = @dictionary.count { |word| word.include?(letter)}
      if count > best_count
        best = letter
        best_count = count
      end
    end
    @alphabet.delete(best)
    best
  end



end

if __FILE__ == $PROGRAM_NAME
  dictionary = ComputerPlayer.read_dictionary
  players = {
    guesser: HumanPlayer.new,
    referee: ComputerPlayer.new(dictionary)}
  game = Hangman.new(players)
end
